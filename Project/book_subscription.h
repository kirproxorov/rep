#pragma once
#ifndef SESIE_H
#define SESIE_H
#include <string>
#include "constants.h"

struct date
{
    char d[11];
};

struct person
{
    char first_name[MAX_STRING_SIZE];
    char middle_name[MAX_STRING_SIZE];
    char last_name[MAX_STRING_SIZE];
};

struct Another
{
    person child;
    date start;
    int lil;
    std::string work;
    char title[MAX_STRING_SIZE];
};

#endif