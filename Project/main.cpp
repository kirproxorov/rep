#include <iostream>
#include <iomanip>
#include <iostream>
#include <iomanip>
#include<string.h>

using namespace std;

#include "book_subscription.h"
#include "file_reader.h"
#include "constants.h"

Another** Specail_Direction(Another* array[], int size, bool(*check)(Another* element), int& result_size);

/*
�������� ������� <function_name>:
    ������� ���������� ������ � ��������� ������� � ��� ��������� �� ��������,
    ��� ������� ������� ������ ���������� �������� true, ���������� � �����
    ������, ��������� �� ������� ������������ ��������

���������:
    array       - ������ � ��������� �������
    size        - ������ ������� � ��������� �������
    check       - ��������� �� ������� ������.
                  � �������� �������� ����� ��������� ����� �������� ���
                  ������� ������, �������� ������� ������� ����
    result_data - ��������, ������������ �� ������ - ����������, � �������
                  ������� ������� ������ ��������������� �������

������������ ��������
    ��������� �� ������ �� ���������� �� ��������, ��������������� �������
    ������ (��� ������� ������� ������ ���������� true)
*/


bool check_lil(Another* element);
bool check_work(Another* element);


/*
�������� ������� <check_function_name>:
    ������� ������ - ���������, ������������� �� ���� ������� ������� ������

���������:
    element - ��������� �� �������, ������� ����� ���������

������������ ��������
    true, ���� ������� ������������� ������� ������, � false � ���� ������
*/

int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "������������ ������ �8. GIT\n";
    cout << "������� �8. ����� ������\n";
    cout << "�����: �������� ������\n\n";
    cout << "������: 11\n";
    Another* subscriptions[100];
    int size;
    try
    {
        read("data.txt", subscriptions, size);
        cout << "***** ����� ������ *****\n\n";
        for (int i = 0; i < size; i++)
        {
            cout << "������\n";
            cout << subscriptions[i]->child.last_name << " ";
            cout << subscriptions[i]->child.first_name[0] << ". ";
            cout << subscriptions[i]->child.middle_name[0] << ".";
            cout << '\n';
            cout << "����\n";
            cout << subscriptions[i]->start.d << " ";
            //cout << ", ";
            ///*cout << '"' << subscriptions[i]->title << '"';*/
            cout << '\n';
            cout << "������\n";
            cout << subscriptions[i]->lil;
            cout << '\n';
            cout << "�������\n";
            cout << setw(4) << setfill('0') << subscriptions[i]->work;
            cout << '\n';
            cout << '\n';
        }
        /* for (int i = 0; i < size; i++)
         {
             delete subscriptions[i];
         }*/

        int choose = 0;
        cin >> choose;
        int result_size;
        Another** subscriptions2;
        if (choose == 1)
            subscriptions2 = Specail_Direction(subscriptions, size, check_lil, result_size);
        else
            subscriptions2 = Specail_Direction(subscriptions, size, check_work, result_size);


        for (int i = 0; i < result_size; i++)
        {
            cout << "������\n";
            cout << subscriptions[i]->child.last_name << " ";
            cout << subscriptions[i]->child.first_name << ". ";
            cout << subscriptions[i]->child.middle_name << ".";
            cout << '\n';
            cout << "����\n";
            cout << subscriptions[i]->start.d << " ";
            //cout << ", ";
            ///*cout << '"' << subscriptions[i]->title << '"';*/
            cout << '\n';
            cout << "������\n";
            cout << subscriptions[i]->lil;
            cout << '\n';
            cout << "�������\n";
            cout << subscriptions[i]->work;
            cout << '\n';
            cout << '\n';
        }

        delete[] subscriptions2;

        for (int i = 0; i < size; i++)
        {
            delete subscriptions[i];
        }

    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}

Another** Specail_Direction(Another* array[], int size, bool(*check)(Another* element), int& result_size)
{
    Another** result = new Another * [size];
    result_size = 0;

    for (int i = 0; i < size; ++i)
        if (check(array[i]))
        {
            result[i] = new Another;
            result[result_size++] = array[i];
        }

    return result;

}

bool check_lil(Another* element)
{
    if (element->lil <= 7)
        return true;
    return false;

}

bool check_work(Another* element)
{
    if (element->work == "�������")
        return true;
    return false;

}
